// ------------- CREATE BOTH TABLES---------------

const { connection } = require('../../utils/connection');
const jsonData = require('../../movies-data/movies.json');

// drop the tables if it exists

const dropTable = tableName => new Promise((resolve, reject) => {
  const sql = `DROP TABLE IF EXISTS ${tableName}`;
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// making director table

const createDirectorTable = () => {
  const sql = 'CREATE TABLE directorData(Id INT NOT NULL PRIMARY KEY AUTO_INCREMENT , Director_Name VARCHAR(50))';
  return new Promise((resolve, reject) => {
    connection.query(sql, (err, res) => {
      if (err) reject(err);
      resolve(res);
    });
  });
};

// get all unique director name from jsonData

const uniqueDirector = () => {
  const director = new Set();
  jsonData.forEach((data) => {
    director.add(data.Director);
  });
  const directorArray = [];
  director.forEach((item) => {
    directorArray.push({
      Director: item,
    });
  });
  return directorArray;
};

// Insert all unique director into its director table

const insertValueIntoDirector = () => {
  const uniqueDirectorName = uniqueDirector();
  const promise = uniqueDirectorName.map(element => new Promise((resolve, reject) => {
    connection.query(`insert into directorData (Director_Name) VALUES ("${element.Director}");`, (error, res) => {
      if (error) reject(error);
      resolve(res);
    });
  }));
  Promise.all(promise);
};

// create movies table

const createMovieTable = () => new Promise((resolve, reject) => {
  const sql = 'CREATE TABLE moviesData(movie_id int NOT NULL PRIMARY KEY AUTO_INCREMENT, Rank int NOT NULL, Title varchar(50), Description varchar(255), Runtime int, Genre varchar(10), Rating float, Metascore varchar(10), Votes int, Gross_Earning_in_Mil varchar(10), Actor varchar(50), Year int, Director_id INT NOT NULL , FOREIGN KEY(Director_id) REFERENCES directorData(Id) ON DELETE CASCADE ON UPDATE CASCADE)';
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// get id of director

const getID = name => new Promise((resolve, reject) => {
  const getid = `SELECT * FROM directorData WHERE Director_Name = "${name}"`;
  connection.query(getid, (err, res) => {
    if (err) reject(err);
    resolve(res[0].Id);
  });
});

// insert values into movies table and director id from director table

const valueInsertedIntoMOviesTable = (data) => {
  const promise = data.map(element => new Promise((resolve, reject) => {
    const directname = getID(element.Director);
    directname.then((val) => {
      const sqlquery = `INSERT INTO moviesData(Rank, Title, Description, Runtime, Genre, Rating, Metascore, Votes, Gross_Earning_in_Mil, Actor, Year, Director_id) values(${element.Rank}, "${element.Title}", "${element.Description}", ${element.Runtime}, "${element.Genre}", ${element.Rating}, "${element.Metascore}", ${element.Votes}, "${element.Gross_Earning_in_Mil}", "${element.Actor}", ${element.Year}, ${val})`;
      connection.query(sqlquery, (err, res) => {
        if (err) reject(err);
        resolve(res);
      });
    });
  }));
  Promise.all(promise);
};

// print tables

async function populate() {
  await dropTable('moviesData');
  await dropTable('directorData');
  await createDirectorTable();
  await insertValueIntoDirector();
  await createMovieTable();
  await valueInsertedIntoMOviesTable(jsonData);
}

populate();
