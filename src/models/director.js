
const { connection } = require('../../utils/connection');
// const jsonData = require('../../movies-data/movies.json');

// ------ query(1)-----

const getAllDirector = () => new Promise((resolve, reject) => {
  const sql = 'SELECT * FROM directorData';
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
  console.log('get all the directors');
});

// ------ query(2)-----

const getDirectorNameWithId = id => new Promise((resolve, reject) => {
  const sql = `SELECT Director_Name FROM directorData WHERE Id = ${id}`;
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
  console.log('get director name with given id');
});

// ------ query(3)-----

const addNewDirector = directorObject => new Promise((resolve, reject) => {
  const sql = 'INSERT INTO directorData SET ?';
  connection.query(sql, directorObject, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// ------ query(4)-----

const updateDirectorName = (name, id) => new Promise((resolve, reject) => {
  const sql = `UPDATE directorData SET ? WHERE Id = ${id}`;
  connection.query(sql, name, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// ------ query(5)-----

const deleteDirector = id => new Promise((resolve, reject) => {
  const sql = `DELETE FROM directorData WHERE Id = ${id}`;
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// getAllDirector().then(item => console.log(item));
// getDirectorNameWithId(5).then(item => console.log(item));
// addNewDirector('rahul').then(item => console.log(item));
// updateDirectorName('yatin', 36).then(item => console.log(item));
// deleteDirector(36);

module.exports.getAllDirector = getAllDirector;
module.exports.getDirectorNameWithId = getDirectorNameWithId;
module.exports.addNewDirector = addNewDirector;
module.exports.updateDirectorName = updateDirectorName;
module.exports.deleteDirector = deleteDirector;
