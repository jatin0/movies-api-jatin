
const { connection } = require('../../utils/connection');
// const jsonData = require('../../movies-data/movies.json');

const getAllMovies = () => new Promise((resolve, reject) => {
  const sql = 'SELECT moviesData.*, directorData.Director_Name FROM directorData INNER JOIN moviesData On directorData.Id = moviesData.Director_id';
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// ------ query(7)-----

const getMoviesWithGivenId = id => new Promise((resolve, reject) => {
  const sql = `SELECT * FROM moviesData WHERE movie_id = ${id}`;
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// ------ query(8)-----

const addNewMovie = moviesDetailObject => new Promise((resolve, reject) => {
  // const checkId = getMoviesWithGivenId(`${directorId}`);
  // checkId.then(item => console.log(item.length));
  // checkId.then((item) => {
  // if (item.length > 0) {
  const addValue = 'INSERT INTO moviesData SET ?';
  connection.query(addValue, moviesDetailObject, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
  // } else { console.log('Invalid Director Id......outside'); }
});
// });

// ------ query(9)-----

const updateMovie = (movieId, movieContentObject) => new Promise((resolve, reject) => {
  const updatequery = `UPDATE moviesData SET ? WHERE movie_id = ${movieId}`;
  connection.query(updatequery, movieContentObject, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// ------ query(10)-----

const deletemovie = id => new Promise((resolve, reject) => {
  const deletequery = `DELETE FROM moviesData WHERE movie_id = ${id}`;
  connection.query(deletequery, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// getAllMovies().then(item => console.log(item));
// getMoviesWithGivenId(3).then(item => console.log(item));
// addNewMovie({'Rank': 10, 'Title': 'NA', 'Description': 'NA', 'Runtime': 12, 'Genre': 'NA', 'Rating': 6.5, 'Metascore': 'NA', 'votes': 0, 'Gross_Earning_in_Mil': 'NA', 'Actor': 'NA', 'Year': 2100, 'Director_id': 32}).then(item => console.log(item));
// updateMovie(51, { Genre: 'crime', Actor: 'yatin', Year: 2000 }).then(item => console.log(item));
// deletemovie(64);


module.exports.getAllMovies = getAllMovies;
module.exports.getMoviesWithGivenId = getMoviesWithGivenId;
module.exports.addNewMovie = addNewMovie;
module.exports.updateMovie = updateMovie;
module.exports.deletemovie = deletemovie;
