const express = require('express');
const Joi = require('@hapi/joi');
const director = require('../models/director');


const app = express();

app.use(express.json());

const schema = Joi.object({
  Director_Name: Joi.string().min(1).required(),
});

// get request of all director

app.get('/director', async (req, res) => {
  const getDirector = await director.getAllDirector();
  res.send(getDirector);
});

// get the director name by its id

app.get('/director/:id', async (req, res) => {
  const { id } = req.params;
  const checkid = await director.getDirectorNameWithId(id);
  if (checkid.length === 0) res.status(404).send('Invalid movie id');
  else {
    res.send(checkid);
  }
});

// add new director

app.post('/director', async (req, res) => {
  const validate = await Joi.validate(req.body, schema).then(resolve => resolve).catch(rej => rej);
  // console.log(validate);
  if (validate.name === 'ValidationError') { res.status(404).json(validate); } else {
    await director.addNewDirector(req.body);
    res.status(201).send(`director added with name: ${req.body.Director_Name}`);
  }
});

// update director name with given id

app.put('/director/:id', async (req, res) => {
  const { id } = req.params;
  const validate = await Joi.validate(req.body, schema).then(resolve => resolve).catch(rej => rej);
  if (validate.name === 'ValidationError') { res.status(404).json(validate); } else {
    const checkid = await director.updateDirectorName(id);
    if (checkid.length === 0) res.status(404).send('Invalid director id');
    else {
      await director.updateDirectorName(req.body, id);
      res.send('director name updated successfully');
    }
  }
});

// delete director with the given id

app.delete('/director/:id', async (req, res) => {
  const { id } = req.params;
  const checkid = await director.deleteDirector(id);
  if (checkid.length === 0) res.status(404).send('Invalid director id');
  else {
    await director.deleteDirector(id);
    res.send('director deleted successfully');
  }
});

app.listen(4000, console.log('server conneted .... enjoy'));
