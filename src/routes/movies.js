const express = require('express');
const Joi = require('@hapi/joi');
const movies = require('../models/movies');

const app = express();

app.use(express.json());

const schemaForInsert = Joi.object({
  Rank: Joi.number(),
  Title: Joi.string().min(1),
  Description: Joi.string().min(1),
  Runtime: Joi.number(),
  Genre: Joi.string().min(1),
  Rating: Joi.number(),
  Metascore: Joi.string().min(1),
  Votes: Joi.number(),
  Gross_Earning_in_Mil: Joi.string().min(1),
  Actor: Joi.string().min(1),
  Year: Joi.number(),
  Director_id: Joi.number(),
});

const schema = Joi.object({
  Rank: Joi.number().positive().required(),
  Title: Joi.string().min(1).required(),
  Description: Joi.string().min(1).required(),
  Runtime: Joi.number().positive().required(),
  Genre: Joi.string().min(1).required(),
  Rating: Joi.number().positive().required(),
  Metascore: Joi.string().min(1).required(),
  Votes: Joi.number().positive().required(),
  Gross_Earning_in_Mil: Joi.string().min(1).required(),
  Actor: Joi.string().min(1).required(),
  Year: Joi.number().positive().required(),
  Director_id: Joi.number().positive().required(),
});

// get request of all movies

app.get('/movies', async (req, res) => {
  const getMoviesDetail = await movies.getAllMovies();
  res.send(getMoviesDetail);
});

// get the movies name by its id

app.get('/movies/:id', async (req, res) => {
  const { id } = req.params;
  const checkid = await movies.getMoviesWithGivenId(id);
  if (checkid.length === 0) res.status(404).send('Invalid movie id');
  else {
    res.send(checkid);
  }
});

// add new movies

app.post('/movies', async (req, res) => {
  const validate = await Joi.validate(req.body, schemaForInsert)
    .then(resolve => resolve)
    .catch(rej => rej);
  if (validate.name === 'ValidationError') { res.status(404).json(validate); } else {
    await movies.addNewMovie(req.body);
    res.status(201).send(`movies added with tile name: ${req.body.Title}`);
  }
});

// update movies name with given id

app.put('/movies/:id', async (req, res) => {
  const { id } = req.params;
  const validate = await Joi.validate(req.body, schema).then(resolve => resolve).catch(rej => rej);
  if (validate.name === 'ValidationError') { res.status(404).json(validate); } else {
    const checkid = await movies.getMoviesWithGivenId(id);
    if (checkid.length === 0) res.status(404).send('Invalid movie id');
    else {
      await movies.updateMovie(id, req.body);
      res.send('movies updated successfully');
    }
  }
});

// delete movies with the given id

app.delete('/movies/:id', async (req, res) => {
  const { id } = req.params;
  const checkid = await movies.getMoviesWithGivenId(id);
  if (checkid.length === 0) res.status(404).send('Invalid movie id');
  else {
    await movies.deletemovie(id);
    res.send('movie deleted successfully');
  }
});

app.listen(4000, console.log('server conneted to movies.... enjoy'));
