const express = require('express');
const { connection } = require('./utils/connection');

const app = express();


app.get('/', (req, res) => {
  res.send('hello world');
});

app.get('/home', (req, res) => {
  res.send('hello page');
});

app.listen(3002, console.log('server started'));

connection.connect((err) => {
  if (err) throw err;
  console.log('Connected!');
});

const jsonData = require('./movies-data/movies.json');
// drop the tables if it exists

const dropTable = tableName => new Promise((resolve, reject) => {
  const sql = `DROP TABLE IF EXISTS ${tableName}`;
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// making director table

const createDirectorTable = () => {
  const sql = 'CREATE TABLE directorData(Id INT NOT NULL PRIMARY KEY AUTO_INCREMENT , Director_Name VARCHAR(50))';
  return new Promise((resolve, reject) => {
    connection.query(sql, (err, res) => {
      if (err) reject(err);
      resolve(res);
    });
  });
};

// get all unique director name from jsonData

const uniqueDirector = () => {
  const director = new Set();
  jsonData.forEach((data) => {
    director.add(data.Director);
  });
  const directorArray = [];
  director.forEach((item) => {
    directorArray.push({
      Director: item,
    });
  });
  return directorArray;
};

// Insert all unique director into its director table

const insertValueIntoDirector = () => {
  const uniqueDirectorName = uniqueDirector();
  const promise = uniqueDirectorName.map(element => new Promise((resolve, reject) => {
    connection.query(`insert into directorData (Director_Name) VALUES ("${element.Director}");`, (error, res) => {
      if (error) reject(error);
      resolve(res);
    });
  }));
  Promise.all(promise);
};

// create movies table

const createMovieTable = () => new Promise((resolve, reject) => {
  const sql = 'CREATE TABLE moviesData(movie_id int NOT NULL PRIMARY KEY AUTO_INCREMENT, Rank int NOT NULL, Title varchar(50), Description varchar(255), Runtime int, Genre varchar(10), Rating float, Metascore varchar(10), Votes int, Gross_Earning_in_Mil varchar(10), Actor varchar(50), Year int, Director_id INT NOT NULL , FOREIGN KEY(Director_id) REFERENCES directorData(Id) ON DELETE CASCADE ON UPDATE CASCADE)';
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// get id of director

const getID = name => new Promise((resolve, reject) => {
  const getid = `SELECT * FROM directorData WHERE Director_Name = "${name}"`;
  connection.query(getid, (err, res) => {
    if (err) reject(err);
    resolve(res[0].Id);
  });
});

// insert values into movies table and director id from director table

const valueInsertedIntoMOviesTable = (data) => {
  const promise = data.map(element => new Promise((resolve, reject) => {
    const directname = getID(element.Director);
    directname.then((val) => {
      const sqlquery = `INSERT INTO moviesData(Rank, Title, Description, Runtime, Genre, Rating, Metascore, Votes, Gross_Earning_in_Mil, Actor, Year, Director_id) values(${element.Rank}, "${element.Title}", "${element.Description}", ${element.Runtime}, "${element.Genre}", ${element.Rating}, "${element.Metascore}", ${element.Votes}, "${element.Gross_Earning_in_Mil}", "${element.Actor}", ${element.Year}, ${val})`;
      connection.query(sqlquery, (err, res) => {
        if (err) reject(err);
        resolve(res);
      });
    });
  }));
  Promise.all(promise);
};

// ------ query(1)-----

const getAllDirector = () => new Promise((resolve, reject) => {
  const sql = 'SELECT Director_Name FROM directorData';
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
  console.log('get all the directors');
});

// ------ query(2)-----

const getDirectorNameWithId = id => new Promise((resolve, reject) => {
  const sql = `SELECT Director_Name FROM directorData WHERE Id = ${id}`;
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
  console.log('get director name with given id');
});

// ------ query(3)-----

const addNewDirector = name => new Promise((resolve, reject) => {
  const sql = `INSERT INTO directorData(Director_Name)VALUES('${name}')`;
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// ------ query(4)-----

const updateDirectorName = (name, id) => new Promise((resolve, reject) => {
  const sql = `UPDATE directorData SET Director_Name = "${name}" WHERE Id = ${id}`;
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// ------ query(5)-----

const deleteDirector = id => new Promise((resolve, reject) => {
  const sql = `DELETE FROM directorData WHERE Id = ${id}`;
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// ------ query(6)-----

const getAllMovies = () => new Promise((resolve, reject) => {
  const sql = 'SELECT *, Director_Name FROM moviesData INNER JOIN directorData On moviesData.Director_id = directorData.Id';
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// ------ query(7)-----

const getMoviesWithGivenId = id => new Promise((resolve, reject) => {
  const sql = `SELECT * FROM moviesData WHERE movie_id = ${id}`;
  connection.query(sql, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// ------ query(8)-----

const addNewMovie = (rank, title, description, runtime, genre, rating,
  metascore, votes, grossEarning, actor, year, directorId) => new Promise((resolve, reject) => {
  const checkId = getDirectorNameWithId(`${directorId}`);
  checkId.then(item => console.log(item.length));
  checkId.then((item) => {
    if (item.length > 0) {
      const addValue = `INSERT INTO moviesData(Rank, Title, Description, Runtime, Genre, Rating, Metascore, Votes, Gross_Earning_in_Mil, Actor, Year, Director_id) 
      values(${rank}, "${title}", "${description}", ${runtime}, "${genre}", ${rating}, "${metascore}", ${votes}, "${grossEarning}", "${actor}", ${year}, ${directorId})`;
      connection.query(addValue, (err, res) => {
        if (err) { console.log('Invalid Director Id.......inside'); }
        resolve(res);
      });
    } else { console.log('Invalid Director Id......outside'); }
  });
});

// ------ query(9)-----

const updateMovie = (movieId, movieContentObject) => new Promise((resolve, reject) => {
  const updatequery = `UPDATE moviesData SET ? WHERE movie_id = ${movieId}`;
  connection.query(updatequery, movieContentObject, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

// updateMovie(50, {Genre:'crime', Year:2000});

// ------ query(10)-----

const deletemovie = id => new Promise((resolve, reject) => {
  const deletequery = `DELETE FROM moviesData WHERE movie_id = ${id}`;
  connection.query(deletequery, (err, res) => {
    if (err) reject(err);
    resolve(res);
  });
});

async function populate() {
  await dropTable('moviesData');
  await dropTable('directorData');
  await createDirectorTable();
  await insertValueIntoDirector();
  await createMovieTable();
  await valueInsertedIntoMOviesTable(jsonData);
}

// populate();
// getAllDirector().then(item => console.log(item));
// getDirectorNameWithId(5).then(item => console.log(item));
// addNewDirector('rahul').then(item => console.log(item));
// updateDirectorName('yatin', 36).then(item => console.log(item));
// deleteDirector(36);
// getAllMovies().then(item => console.log(item));
// getMoviesWithGivenId(3).then(item => console.log(item));
// addNewMovie(10, 'NA', 'NA', 12, 'NA', 6.5, 'NA', 0, 'NA', 'NA', 2100, 32).then(item => console.log(item));
// updateMovie(51, { Genre: 'crime', Actor: 'yatin', Year: 2000 }).then(item => console.log(item));
// deletemovie(64);
